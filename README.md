# Payum BuyPlan Gateway

## Installation
```
composer require buyplan-estonia/payum-buyplan 
```

## Basic Usage

```php
<?php

use Payum\Core\PayumBuilder;
use Payum\Core\GatewayFactoryInterface;

$defaultConfig = [];

$payum = (new PayumBuilder)
    ->addGatewayFactory('buyplan', function(array $config, GatewayFactoryInterface $coreGatewayFactory) {
        return new \BuyPlanEstonia\PayumBuyPlan\BuyPlanGatewayFactory($config, $coreGatewayFactory);
    })

    ->addGateway('buyplan', [
        'factory' => 'buyplan',
        'testing' => true,
    ])

    ->getPayum()
;
```

### Currently implemented `Action`s:
- `StatusAction`
- `ConvertPaymentAction`
- `CaptureAction`

```php
<?php

use Payum\Core\Request\Capture;

$BuyPlan = $payum->getGateway('buyplan');

$model = new \ArrayObject([
  // ...
]);

$BuyPlan->execute(new Capture($model));
```

## Resources

* [Documentation](https://github.com/Payum/Payum/blob/master/docs/index.md#general)
* [Questions](http://stackoverflow.com/questions/tagged/payum)
* [Issue Tracker](https://github.com/Payum/Payum/issues)
* [Twitter](https://twitter.com/payumphp)

## License

This software is released under the [MIT License](LICENSE).
