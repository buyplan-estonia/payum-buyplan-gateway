<?php
namespace BuyPlanEstonia\PayumBuyPlan\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Request\Capture;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\RenderTemplate;
use Payum\Core\Security\TokenInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class CaptureAction implements ActionInterface, GatewayAwareInterface, LoggerAwareInterface
{
    use GatewayAwareTrait;
    use LoggerAwareTrait;

    private $BuyPlanAPIURL;
    private $templateName;
    private $merchantRegistryCode;
    private $merchantPrivateKey;
    private $BuyPlanPublicKey;

    private $requestTypes = [
        1012 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_STAMP', 'VK_AMOUNT', 'VK_CURR', 'VK_REF', 'VK_MSG', 'VK_RETURN', 'VK_CANCEL', 'VK_DATETIME', 'VK_ORDER', 'VK_MAC', 'VK_ENCODING', 'VK_LANG'],
        1111 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_REC_ID', 'VK_STAMP', 'VK_T_NO', 'VK_AMOUNT', 'VK_CURR', 'VK_REC_ACC', 'VK_REC_NAME', 'VK_SND_ACC', 'VK_SND_NAME', 'VK_REF', 'VK_MSG', 'VK_T_DATETIME', 'VK_MAC', 'VK_ENCODING', 'VK_LANG', 'VK_AUTO'],
        1911 => ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID', 'VK_REC_ID', 'VK_STAMP', 'VK_REF', 'VK_MSG', 'VK_MAC', 'VK_ENCODING', 'VK_LANG', 'VK_AUTO'],
    ];

    public function __construct(
        $BuyPlanAPIURL,
        $templateName,
        $merchantRegistryCode,
        $merchantPrivateKey,
        $BuyPlanPublicKey)
    {
        $this->BuyPlanAPIURL = $BuyPlanAPIURL;
        $this->templateName = $templateName;
        $this->merchantRegistryCode = $merchantRegistryCode;
        $this->merchantPrivateKey = $merchantPrivateKey;
        $this->BuyPlanPublicKey = $BuyPlanPublicKey;
    }

    /**
     * {@inheritDoc}
     *
     * @param Capture $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        $getHttpRequest = new GetHttpRequest();
        $this->gateway->execute($getHttpRequest);

        if (isset($getHttpRequest->query['done']) && $getHttpRequest->query['done']) {
            $this->logger->info('Query succeeded, verifying response...');
            if (!$this->requestHasValidMAC($getHttpRequest->request)) {
                $this->logger->info('Could not verify response, updating status...');
                $model['status'] = 'failed';
                return;
            }

            $this->logger->info('Success-response verified, updating status...');
            $model['status'] = 'done';
            return;
        }

        if (isset($getHttpRequest->query['cancel']) && $getHttpRequest->query['cancel']) {
            $this->logger->info('Query cancelled, verifying response...');
            if (!$this->requestHasValidMAC($getHttpRequest->request)) {
                $this->logger->info('Could not verify response, updating status...');
                $model['status'] = 'failed';
                return;
            }

            $this->logger->info('Cancel-response verified, updating status...');
            $model['status'] = 'cancelled';
            return;
        }

        $fields = $this->getRequestFields($model, $request->getToken());
        $this->gateway->execute($renderTemplate = new RenderTemplate($this->templateName, [
            'fields' => $fields,
            'url' => $this->BuyPlanAPIURL,
        ]));

        throw new HttpResponse($renderTemplate->getResult());
    }

    private function getRequestFields($paymentRequestModel, TokenInterface $token)
    {
        $paymentTimeStamp = gmdate('Y-m-d\TH:i:sO');

        $paymentRequestFields = [
            'VK_SERVICE' => '1012',
            'VK_VERSION' => '008',
            'VK_SND_ID' => $this->merchantRegistryCode,
            'VK_STAMP' => $paymentRequestModel['reference'],
            'VK_AMOUNT' => $paymentRequestModel['amount'],
            'VK_CURR' => $paymentRequestModel['currency'],
            'VK_REF' => '',
            'VK_MSG' => $paymentRequestModel['message'],
            'VK_RETURN' => $token->getTargetUrl().'?done=1',
            'VK_CANCEL' => $token->getTargetUrl().'?cancel=1',
            'VK_DATETIME' => $paymentTimeStamp,
            'VK_ORDER' => $paymentRequestModel['order'],
            'VK_ENCODING' => 'UTF-8',
            'VK_LANG' => 'EST',
        ];

        $paymentRequestFields['VK_MAC'] = $this->getMAC($paymentRequestFields, $this->merchantPrivateKey);
        return $paymentRequestFields;
    }

    private function getMAC($paymentRequestFields, $merchantPrivateKey)
    {
        // using strlen() because we count bytes
        $message = '';
        foreach ($this->requestTypes[$paymentRequestFields['VK_SERVICE']] as $paymentRequestField)
        {
            if (strcmp($paymentRequestField, 'VK_MAC') == 0) continue;
            if (strcmp($paymentRequestField, 'VK_ENCODING') == 0) continue;
            if (strcmp($paymentRequestField, 'VK_LANG') == 0) continue;
            if (strcmp($paymentRequestField, 'VK_AUTO') == 0) continue;
            $message .= str_pad(strval(strlen($paymentRequestFields[$paymentRequestField])), 3, '0', 0) . $paymentRequestFields[$paymentRequestField];
        }

        $MAC = '';
        openssl_sign($message, $MAC, $merchantPrivateKey, OPENSSL_ALGO_SHA1);
        return base64_encode($MAC);
    }

    public function requestHasValidMAC($request): bool
    {
        if (!$this->requestTypes[$request['VK_SERVICE']])
        {
            $this->logger->info('Unexpected query type: '.$request['VK_SERVICE']);
            return false;
        }

        // using strlen() because we count bytes
        $message = '';
        foreach ($this->requestTypes[$request['VK_SERVICE']] as $paymentRequestField)
        {
            if (strcmp($paymentRequestField, 'VK_MAC') == 0) continue;
            if (strcmp($paymentRequestField, 'VK_ENCODING') == 0) continue;
            if (strcmp($paymentRequestField, 'VK_LANG') == 0) continue;
            if (strcmp($paymentRequestField, 'VK_AUTO') == 0) continue;
            $message .= str_pad(strval(strlen($request[$paymentRequestField])), 3, '0', 0) . $request[$paymentRequestField];
        }

        if (1 == (openssl_verify($message, base64_decode($request['VK_MAC']), openssl_pkey_get_public($this->BuyPlanPublicKey)))) return true;
        else return false;
    }


    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof \ArrayAccess
        ;
    }
}
