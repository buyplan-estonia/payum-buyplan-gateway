<?php
declare(strict_types = 1);

namespace BuyPlanEstonia\PayumBuyPlan\Action\Api;

use Payum\Core\Exception\UnsupportedApiException;
use BuyPlanEstonia\PayumBuyPlan\BuyPlanGatewayAPI;

trait ApiAwareTrait
{
    /** @var BuyPlanGatewayAPI */
    protected $api;

    /**
     * @param BuyPlanGatewayAPI $api
     */
    public function setApi($api): void
    {
        if (!$api instanceof BuyPlanGatewayAPI) {
            throw new UnsupportedApiException(sprintf('Given API is not an instance of %s', BuyPlanGatewayAPI::class));
        }

        $this->api = $api;
    }
}