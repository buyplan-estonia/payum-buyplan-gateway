<?php
namespace BuyPlanEstonia\PayumBuyPlan\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Request\GetStatusInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class StatusAction implements ActionInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * {@inheritDoc}
     *
     * @param GetStatusInterface $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        if (!isset($model['status'])) {
            $this->logger->info('Marking request as new...');
            $request->markNew();
            return;
        } elseif ($model['status'] == 'done') {
            $this->logger->info('Marking request as captured...');
            $request->markCaptured();
            return;
        } elseif ($model['status'] == 'cancelled') {
            $this->logger->info('Marking request as cancelled...');
            $request->markCanceled();
            return;
        } elseif ($model['status'] == 'failed') {
            $this->logger->info('Marking request as failed...');
            $request->markFailed();
            return;
        } else {
            $this->logger->info('Unexpected payment status, marking request as unknown...');
            $request->markUnknown();
            return;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return $request instanceof GetStatusInterface && $request->getModel() instanceof \ArrayAccess;
    }
}
