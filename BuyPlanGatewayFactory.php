<?php
namespace BuyPlanEstonia\PayumBuyPlan;

use BuyPlanEstonia\PayumBuyPlan\Action\AuthorizeAction;
use BuyPlanEstonia\PayumBuyPlan\Action\CancelAction;
use BuyPlanEstonia\PayumBuyPlan\Action\CaptureAction;
use BuyPlanEstonia\PayumBuyPlan\Action\NotifyAction;
use BuyPlanEstonia\PayumBuyPlan\Action\RefundAction;
use BuyPlanEstonia\PayumBuyPlan\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

class BuyPlanGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        if (false == $config['payum.api']) {
            $config['payum.default_options'] = array(
                'testing' => $config['testing'],
                'merchant_registry_code' => $config['merchant_registry_code'],
                'merchant_private_key' => $config['merchant_private_key'],
                'buyplan_public_key' => $config['buyplan_public_key'],
                'live_api_url' => 'https://app.buyplan.ee/api/merchant/payment',
                'test_api_url' => 'https://app.test.buyplan.ee/api/merchant/payment',
                'payum.template.client_side_redirect' => '@BuyPlanEstonia/client_side_redirect.html.twig'
            );
            $config->defaults($config['payum.default_options']);

            $config->defaults([
                'payum.factory_name' => 'buyplan',
                'payum.factory_title' => 'BuyPlan',
                'payum.action.capture' => new CaptureAction(
                    $config['testing'] ? $config['test_api_url'] : $config['live_api_url'],
                    $config['payum.template.client_side_redirect'],
                    $config['merchant_registry_code'],
                    $config['merchant_private_key'],
                    $config['buyplan_public_key']
            ),
                'payum.action.authorize' => new AuthorizeAction(),
                'payum.action.refund' => new RefundAction(),
                'payum.action.cancel' => new CancelAction(),
                'payum.action.notify' => new NotifyAction(),
                'payum.action.status' => new StatusAction(),
            ]);

            $config['payum.required_options'] = [
                'buyplan_public_key',
                'merchant_private_key',
                'merchant_registry_code',
            ];

            $config['payum.api'] = function (ArrayObject $config) {
                $config->validateNotEmpty($config['payum.required_options']);

                return new BuyPlanGatewayAPI((array) $config, $config['payum.http_client'], $config['httplug.message_factory']);
            };

            $config['payum.paths'] = array_replace([
                'BuyPlanEstonia' => __DIR__.'/Resources/views',
            ], $config['payum.paths'] ?: []);
        }
    }
}
